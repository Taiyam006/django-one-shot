# Generated by Django 4.2.1 on 2023-05-31 17:35

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0003_todolist_delete_todoslist"),
    ]

    operations = [
        migrations.RenameModel(
            old_name="TodoList",
            new_name="TodosList",
        ),
    ]
